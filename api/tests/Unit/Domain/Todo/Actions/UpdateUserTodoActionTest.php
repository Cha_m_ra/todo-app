<?php

namespace Tests\Domain\Todo\Action;

use App\Models\Todo;
use Domain\Todo\Actions\UpdateUserTodoAction;
use Tests\TestCase;

class UpdateUserTodoActionTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_get_update_data_when_only_description_is_available()
    {
        $updateUserTodoAction = new UpdateUserTodoAction();
        $status = null;
        $description = fake()->text();
        $expected = ['description' => $description];

        $result = $updateUserTodoAction->getUpdateData(
            description: $description,
            status: $status,
        );

        $this->assertEquals($expected, $result);
    }

    public function test_get_update_data_when_only_status_is_available()
    {
        $updateUserTodoAction = new UpdateUserTodoAction();
        $status = Todo::COMPLETED;
        $description = null;
        $expected = ['status' => $status];

        $result = $updateUserTodoAction->getUpdateData(
            description: $description,
            status: $status,
        );

        $this->assertEquals($expected, $result);
    }

    public function test_get_update_data_when_status_description_is_available()
    {
        $updateUserTodoAction = new UpdateUserTodoAction();
        $status = Todo::COMPLETED;
        $description = fake()->text();
        $expected = ['status' => $status, 'description' => $description];

        $result = $updateUserTodoAction->getUpdateData(
            description: $description,
            status: $status,
        );

        $this->assertEquals($expected, $result);
    }
}