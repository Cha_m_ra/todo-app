<?php

namespace Tests\Domain\Todo\Action;

use App\Models\Todo;
use App\Models\User;
use Domain\Todo\Actions\DeleteUserTodoAction;
use Domain\Todo\Exceptions\NotAllowedException;
use Tests\TestCase;

class DeleteUserTodoActionTest extends TestCase
{

    public function test_exception_is_throwing_for_not_allowed_user_when_try_update_todo()
    {
        $deleteUserTodoAction = new DeleteUserTodoAction(); //
        $todo = Todo::factory()->create();
        $user = User::factory()->create();
        
        $this->expectException(NotAllowedException::class);
        $deleteUserTodoAction->isUserAuthorized($user,$todo);
    }

}