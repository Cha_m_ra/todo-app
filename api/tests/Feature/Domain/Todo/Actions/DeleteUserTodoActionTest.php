<?php

namespace Tests\Feature\Domain\Todo\Actions;

use App\Models\Todo;
use App\Models\User;
use Domain\Todo\Actions\DeleteUserTodo;
use Domain\Todo\Actions\DeleteUserTodoAction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DeleteUserTodoActionTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_delete_todo()
    {
        $user = User::factory()->create();
        $todo = Todo::factory()->create([
            'user_id' => $user->id,
        ]);

        app(DeleteUserTodoAction::class)($todo, $user);

        $this->assertDatabaseMissing('todos', $todo->only(['id', 'description']));
    }
}
