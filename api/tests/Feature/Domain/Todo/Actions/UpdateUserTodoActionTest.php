<?php

namespace Tests\Feature\Domain\Todo\Actions;

use App\Models\Todo;
use App\Models\User;
use Domain\Todo\Actions\DeleteUserTodo;
use Domain\Todo\Actions\UpdateUserTodoAction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateUserTodoActionTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_update_todo()
    {
        $user = User::factory()->create();
        $todo = Todo::factory()->create([
            'user_id' => $user->id,
        ]);

        $status = Todo::PENDING;
        $description = fake()->text();

        app(UpdateUserTodoAction::class)($todo, $description, $status);

        $this->assertDatabaseHas('todos', ['id' => $todo->id, 'status' => $status, 'description' => $description]);
    }

    public function test_user_can_only_update_status_todo()
    {
        $user = User::factory()->create();
        $todo = Todo::factory()->create([
            'user_id' => $user->id,
            'status' => Todo::COMPLETED
        ]);

        $status = Todo::PENDING;

        app(UpdateUserTodoAction::class)($todo, null, $status);

        $this->assertDatabaseHas('todos', ['id' => $todo->id, 'status' => $status]);
    }
}
