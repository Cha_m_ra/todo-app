<?php

namespace Tests\Feature\Domain\Todo\Actions;

use App\Models\User;
use Domain\Todo\Actions\CreateTodoAction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateTodoActionTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_create_a_post()
    {
        $user = User::factory()->create();
        $description = fake()->text();

        $todoData = [
            'description' => $description,
            'user_id' => $user->id,
        ];

        app(CreateTodoAction::class)($user,$description);

        $this->assertDatabaseHas('todos', $todoData);

    }
}