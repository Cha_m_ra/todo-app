<?php

namespace Tests\Feature\Domain\Todo\Actions;

use App\Models\Todo;
use App\Models\User;
use Domain\Todo\Actions\DeleteUserTodo;
use Domain\Todo\Actions\GetUserTodoForStatusAction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GetUserTodoForStatusActionTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_get_todo_by_status()
    {
        $user = User::factory()->create();
        $todo = Todo::factory(1)->create([
            'user_id' => $user->id,
            'status' => Todo::COMPLETED
        ]);

        $result = app(GetUserTodoForStatusAction::class)($user, Todo::COMPLETED);

        $this->assertEquals($todo->toArray(), $result->toArray());
    }
}
