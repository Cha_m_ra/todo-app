<?php

namespace Tests\Feature\Domain\User\Actions;


use Domain\User\Actions\RegisterUserAction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class RegisterActionTest extends TestCase
{
   use RefreshDatabase;
   public function test_register_user_action()
   {
      $userData = [
         'email' => fake()->email(),
         'name' => fake()->name(),
         'password' => "123123",
      ];

      $response = app(RegisterUserAction::class)($userData);

      $this->assertDatabaseHas('users', $response['user']->toArray());
   }
}