<?php

namespace Tests\Feature\Domain\User\Actions;

use App\Models\User;
use Domain\User\Actions\LoginUserAction;
use Domain\User\Actions\RegisterUserAction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LoginActionTest extends TestCase
{
    use RefreshDatabase;
    public function test_login_user_action()
    {
        $password = "123123";

        $userData = [
            'email' => fake()->email(),
            'password' => bcrypt($password),
        ];

        $user = User::factory()->create($userData);

        $userData['password'] =  $password;

        $response = app(LoginUserAction::class)($userData);

        $this->assertDatabaseHas('users', $response['user']->toArray());
    }
}
