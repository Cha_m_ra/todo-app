<?php

namespace Domain\Todo\Actions;

use App\Models\Todo;
use App\Models\User;

class GetUserTodoForStatusAction
{

    public function __invoke(User $user, int $status)
    {
        return Todo::whereUserId($user->id)
                    ->whereStatus($status)
                    ->get();

    }
}
