<?php

namespace Domain\Todo\Actions;

use App\Models\Todo;
use App\Models\User;
use Domain\Todo\Exceptions\NotAllowedException;
use Illuminate\Support\Facades\DB;

class DeleteUserTodoAction
{

    public function __invoke(Todo $todo, User $user)
    {
        try {
            DB::beginTransaction();

            $this->isUserAuthorized($user, $todo);

            $todo->delete();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function isUserAuthorized(User $user, Todo $todo)
    {
        if ($user->id !== $todo->user_id) {
            throw new NotAllowedException();
        }
    }
}
