<?php

namespace Domain\Todo\Actions;

use App\Models\Todo;
use App\Models\User;

class ViewAllUserTodoAction { 

    public function __invoke(User $user) {
        return Todo::whereUserId($user->id)->get();
    }

}