<?php

namespace Domain\Todo\Actions;

use App\Models\Todo;
use Illuminate\Support\Facades\DB;

class UpdateUserTodoAction
{

    public function __invoke(Todo $todo, string| null $description ,string| null $status )
    {
        try {
            DB::beginTransaction();

            $updateData = $this->getUpdateData($description, $status);

            $todo->update($updateData);

            DB::commit();

            return $todo;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function getUpdateData(string| null $description, string| null  $status) : array
    {
        $data = [];

        if($description !== null) $data['description'] = $description;
        if($status !== null) $data['status'] = $status;
        
        return $data;
    }
}
