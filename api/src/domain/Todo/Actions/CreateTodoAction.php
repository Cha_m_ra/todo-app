<?php

namespace Domain\Todo\Actions;

use App\Models\Todo;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class CreateTodoAction
{

    public function __invoke(User $user, string $description)
    {
        try {
            DB::beginTransaction();
            
            $todo =  $user->todo()->create(['description' => $description, 'status' => Todo::PENDING]);

            DB::commit();

            return $todo;
            
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
