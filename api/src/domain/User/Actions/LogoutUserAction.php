<?php

namespace Domain\User\Actions;

use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;

class LogoutUserAction
{

    /**
     * create a new user
     *
     * @param array $userData
     * @return User
     */
    public function __invoke(Authenticatable | User $user,)
    {
        try {
            
            $user->currentAccessToken()->delete();

        } catch (\Exception $e) {

            throw $e;
        }
    }
}
