<?php

namespace Domain\User\Actions;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class RegisterUserAction {

    /**
     * create a new user
     *
     * @param array $userData
     * @return User
     */
    public function __invoke(array $userData) : array
    {
        try {
            DB::beginTransaction();

            $userData['password'] = bcrypt($userData['password']);

            $user =  User::create($userData);

            DB::commit();

            return [
                'user' => $user,
                'token' => $user->getToken(),
            ];

        } catch (\Exception $e) {
            DB::rollBack();

            throw $e;
        }

    }

}