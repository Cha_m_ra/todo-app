<?php

namespace App\Http\Requests;

use App\Models\Todo;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class TodoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Todo::whereUserId(Auth::user()->id)->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'description' => ['string', 'max:200'],
            'status' => [Rule::in([Todo::COMPLETED, Todo::PENDING])]
        ];
    }
}
