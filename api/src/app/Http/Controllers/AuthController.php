<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use Domain\User\Actions\LoginUserAction;
use Domain\User\Actions\LogoutUserAction;
use Domain\User\Actions\RegisterUserAction;
use Domain\User\Exceptions\LoginFailException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthController extends Controller
{
    public function login(
        LoginRequest $request,
        LoginUserAction $loginUserAction
    ) {

        try {
            return response()->json(
                $loginUserAction($request->validated()),
                Response::HTTP_OK
            );
        } catch (LoginFailException $e) {
            return response()->json(['error' => 'invalid login email or password'], Response::HTTP_FORBIDDEN);
        } catch (\Throwable $th) {
            report($th);
            return response()->json('Failed to login', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function register(
        RegisterRequest $request,
        RegisterUserAction $registerUserAction
    ) {
        try {

            return response()->json(
                $registerUserAction($request->validated()),
                Response::HTTP_OK
            );
        } catch (\Throwable $th) {
            report($th);
            return response()->json('Failed to register', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function logout(
        LogoutUserAction $logoutUserAction
    )
    {
        try {

            $logoutUserAction(auth()->user());

            return response()->json(
                "logout successful",
                Response::HTTP_OK
            );
        } catch (\Throwable $th) {
            report($th);
            return response()->json('Failed to logout', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
