<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TodoCreateRequest;
use App\Http\Requests\TodoRequest;
use App\Http\Requests\TodoUpdateRequest;
use App\Models\Todo;
use Domain\Todo\Actions\CreateTodoAction;
use Domain\Todo\Actions\DeleteUserTodo;
use Domain\Todo\Actions\DeleteUserTodoAction;
use Domain\Todo\Actions\GetUserTodoForStatusAction;
use Domain\Todo\Actions\UpdateUserTodoAction;
use Domain\Todo\Exceptions\NotAllowedException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(
        Request $request,
        GetUserTodoForStatusAction $getUserTodoForStatusAction
    )
    {
        return response()->json(
            $getUserTodoForStatusAction(user : Auth::user(), status : $request->status),
            Response::HTTP_OK
        );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(
        TodoCreateRequest $request,
        CreateTodoAction $createTodoAction
    )
    {
        try {

            return response()->json(
                $createTodoAction(user: Auth::user(), description: $request->description),
                Response::HTTP_OK
            );
            
        } catch (\Throwable $th) {

            report($th);
            return response()->json('Failed to create todo', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(
        TodoUpdateRequest $request, 
        Todo $todo, 
        UpdateUserTodoAction $updateUserTodo
    )
    {
        try {

            return response()->json(
                $updateUserTodo(todo : $todo, description: $request->description, status : $request->status),
                Response::HTTP_OK
            );

        } catch (\Throwable $th) {
            report($th);
            return response()->json('Failed to update todo', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(
        Todo $todo,
        DeleteUserTodoAction $deleteUserTodo
    )
    {
        try {

            $deleteUserTodo(
                user: Auth::user(),
                todo: $todo
            );

            return response()->json('Todo deleted successful', Response::HTTP_OK);
        } catch (NotAllowedException $th) {

            return response()->json('Unauthorize', Response::HTTP_UNAUTHORIZED);
        } catch (\Throwable $th) {

            dd($th);
            return response()->json('Failed to delete todo', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
