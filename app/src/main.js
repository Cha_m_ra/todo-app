import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import store from "./store";
import router from './router'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import mixin from './mixin';


Vue.config.productionTip = false

Vue.use(VueToast);

Vue.mixin(mixin)

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
