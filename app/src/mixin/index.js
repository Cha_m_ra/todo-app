export default {
    methods : {
        notifySuccess (message) {
            this.$toast.open({
                message,
                type : 'success',
                position: "top-right",
            });
        },
        notifyError (message) {
            this.$toast.open({
                message,
                type : 'error',
                position: "top-right",
            });
        },
        notifyWarning (message) {
            this.$toast.open({
                message,
                type : 'warning',
                position: "top-right",
            });
        }
    }
}