import axios from 'axios'

import {
    SetupInterceptors
} from './SetupInterceptors'


const http = axios.create({
    baseURL: process.env.VUE_APP_API_URL
})

SetupInterceptors(http)

export default http