import store from "../store"

const baseURL = process.env.VUE_APP_API_URL

export const SetupInterceptors = http => {

    http.interceptors.request.use(
        config => {
            config.headers['Authorization'] = `Bearer ${store.getters.userToken}`
            config.headers['content-type'] = 'application/json'
            return config
        },
        error => {
            const status = error?.request?.status || 0
            const resBaseURL = error?.request?.config?.baseURL
            if (resBaseURL === baseURL && status === 401) {
               if (localStorage.getItem('token')) {
                  localStorage.clear()
                  window.location.assign('/')
                  return Promise.reject(error)
               } else {
                  return Promise.reject(error)
               }
            }
            return Promise.reject(error)
        }
    )

    http.interceptors.response.use(function(response) {
        return response
    }, function (error) {
        const status = error?.response?.status || 0
        const resBaseURL = error?.response?.config?.baseURL
        if (resBaseURL === baseURL && status === 401) {
            if (localStorage.getItem('token')) {
                localStorage.clear()
                window.location.assign('/')
                return Promise.reject(error)
            } else {
                return Promise.reject(error)
            }
        }
        return Promise.reject(error)
    })
}

export default SetupInterceptors